**************************
The Fill and Stroke Dialog
**************************

|Fill and Stroke dialog icon| :kbd:`Shift` + :kbd:`Ctrl` + :kbd:`F` :menuselection:`Object --> Fill and Stroke`
   
Apart from the options listed under the title of this chapter, this dialog can also be opened by double-clicking on the fields for fill and stroke at the bottom left of the Inkscape window.

The dialog has 3 tabs: one for the :guilabel:`Fill`, one for the :guilabel:`Stroke paint` and
one for the :guilabel:`Stroke style`. The first row of buttons in the tabs for fill and stroke paint are for setting the type of fill or stroke paint that
you want to use:

- |Icon for No Paint| The **cross** needs
  to be active when you do not want to have any fill or stroke paint
  respectively.
- |Icon for Solid Paint| The **first
  square** must be activated if you want to apply a flat, uniform
  color.
- |Icon for linear gradients in Fill and Stroke dialog| The **second
  square** with the gradually changing color must be set when you want
  to use a linear gradient.
- |Icon for radial gradients in Fill and Stroke dialog| The **third
  square** with the brighter color in the middle is for setting a
  radial gradient.
- |Icon for mesh gradients in Fill and Stroke dialog| The **next
  square** with its four quarters is needed for using a mesh gradient.
- |image6| The **patterned square** when active will allow you to apply
  a pattern.
- |Icon for swatches in Fill and Stroke dialog| The **white square** is
  useful when you want to reuse the fill for other objects in the same
  document. It acts like a library.
- |image8| The **question mark** does not set any specific color. The
  object inherits the color of its parent.

Below, you can customize your color - we will return to that in the next
chapter. At the bottom of all 3 tabs, there are two fields that set
values for the whole object:

- :guilabel:`Blur`: when you click, or click-and-drag in this field, you can
  apply a blur to the whole selected object. Be warned: in most cases,
  values greater than 2% are pretty useless.
- :guilabel:`Opacity`: when you click, or click-and-drag in this field, this
  will change the global opacity of the selected object - changing the
  opacity of fill and stroke equally.

.. figure:: images/fill_and_stroke_dialog-fill_tab.png
    :alt: The Fill tab of the Fill and Stroke dialog
    :class: screenshot

    The first tab of the :guilabel:`Fill and Stroke` dialog, when an
    object with a plain light blue fill is selected.

.. figure:: images/fill_and_stroke_dialog-stroke_paint_tab.png
    :alt: The Stroke paint tab of the Fill and Stroke dialog
    :class: screenshot

    In the second tab of the :guilabel:`Fill and Stroke` dialog, the stroke is dark blue…

.. figure:: images/fill_and_stroke_dialog-stroke_style_tab.png
    :alt: The Stroke style tab of the Fill and Stroke dialog
    :class: screenshot

    … and 10 mm wide in the third tab.

.. |Fill and Stroke dialog icon| image:: images/icons/dialog-fill-and-stroke.*
   :class: header-icon
.. |Icon for No Paint| image:: images/icons/paint-none.*
   :class: inline
.. |Icon for Solid Paint| image:: images/icons/paint-solid.*
   :class: inline
.. |Icon for linear gradients in Fill and Stroke dialog| image:: images/icons/paint-gradient-linear.*
   :class: inline
.. |Icon for radial gradients in Fill and Stroke dialog| image:: images/icons/paint-gradient-radial.*
   :class: inline
.. |Icon for mesh gradients in Fill and Stroke dialog| image:: images/icons/paint-gradient-mesh.*
   :class: inline
.. |image6| image:: images/icons/paint-pattern.*
   :class: inline
.. |Icon for swatches in Fill and Stroke dialog| image:: images/icons/paint-swatch.*
   :class: inline
.. |image8| image:: images/icons/paint-unknown.*
   :class: inline
